FROM debian:buster

RUN apt-get update && apt-get install -y \
		biber \
		latexmk \
		make \
		texlive-full \
	&& rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y \
        fonts-liberation \
        cloc \
        wget \
        bzip2 \
        python-chardet \
        python-pkg-resources \
        python-pip \
        python3 \
        curl \
        git \
        zip \
    && pip install Pygments \
    && apt-get remove -y python-pip \
	&& rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/share/texmf/tex/latex/vub \
    && cd /usr/share/texmf/tex/latex/vub \
    && wget -O texlive-vub.tar "https://gitlab.com/rubdos/texlive-vub/repository/archive.tar?ref=master" \
    && tar -xvf texlive-vub.tar --strip 1 \
    && rm texlive-vub.tar

RUN mkdir -p /usr/share/texmf/tex/latex/llncs \
    && cd /usr/share/texmf/tex/latex/llncs \
    && wget -O llncs.zip "ftp://ftp.springernature.com/cs-proceeding/llncs/llncs2e.zip" \
    && unzip -d /usr/share/texmf/tex/latex/llncs llncs.zip \
    && rm llncs.zip

RUN mktexlsr
