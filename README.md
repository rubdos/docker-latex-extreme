# LaTeX Docker image with stuff I like

... maybe you like it too! There's minted in here, and the [VUB LaTeX theme](https://gitlab.com/rubdos/texlive-vub/).

Feel free to use this image; pull from: `registry.gitlab.com/rubdos/docker-latex-extreme:master`.
